import logging from "external:@totates/logging/v1";
import DOMPurify from "external:@totates/dompurify/v2";

const logger = logging.getLogger("@totates/trusted-types/v1");
logger.setLevel("INFO");

function createScriptURL(url) {
    const parsed = new window.URL(url, document.baseURI);
    if (parsed.origin === window.location.origin)
        return url;
    throw new TypeError("invalid URL");
}

// https://github.com/WICG/trusted-types/pull/204
function createURL(dirty) {
    const u = new window.URL(dirty, document.baseURI);
    if (["https:", "http:"].includes(u.protocol))
        return u.href;
    throw new Error("Only http(s): URLs allowed");
}

function createHTML(dirty) {
    return DOMPurify.sanitize(dirty, {
        CUSTOM_ELEMENT_HANDLING: {
            tagNameCheck: tagName => tagName.indexOf("-") !== -1,
            attributeNameCheck: attr => attr.indexOf("-") !== -1,
            allowCustomizedBuiltInElements: false
        }
    });
}

let policy = { createScriptURL, createURL, createHTML };

if (window.trustedTypes && window.trustedTypes.createPolicy) {
    logger.info("browser supports trusted types");
    // TODO: the default policy is needed for libraries we can't change, e.g. maplibre;
    // remove it when libraries allow to pass our policy */
    window.trustedTypes.createPolicy("default", policy);
    policy = window.trustedTypes.createPolicy("totates", policy); // overwrite
}
else
    logger.info("browser does not support trusted types");

// TODO: @totates/events
window.addEventListener("securitypolicyviolation", err => logger.error(err));

export default policy;
